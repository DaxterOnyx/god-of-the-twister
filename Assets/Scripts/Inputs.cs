﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class Inputs : MonoBehaviour
{
	private static Inputs _instance;
	public static Inputs Instance
	{
		get
		{
			if (_instance == null) _instance = FindObjectOfType<Inputs>();
			return _instance;
		}
	}

	private SerialPort arduino = new SerialPort("COM3", 9600);


	// Start is called before the first frame update
	void Start()
	{
		arduino.Open();
	}

	// Update is called once per frame
	void Update()
	{
		if (!arduino.IsOpen)
		{
			Debug.LogError("ARDUINO NOT FOUND");
			arduino.Open();
			arduino.ReadTimeout = 1;
		}

		try
		{
			string readLine = arduino.ReadLine();
			int value = int.Parse(readLine);
			Debug.Log(value);
		}
		catch (System.Exception e)
		{
			Debug.LogWarning("byte received not can be parse.");
		}
	
	}
}