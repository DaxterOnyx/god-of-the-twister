﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	[SerializeField] private PlanetMovement Planet;

	public Transform SpawnReference;
	public float TranslationByEntities = 0.1f;
	private Vector3 DefaultTranslation;
	public float FirstTimeWaiting = 1;
	public float TimeBetweenSpawn = 0.2f;
	[SerializeField] public List<Combinaison> PrefabEntities = new List<Combinaison>();
	private float timeBeforeSpawn;

	// Start is called before the first frame update
	void Start()
	{
		timeBeforeSpawn = FirstTimeWaiting;
		DefaultTranslation = SpawnReference.localPosition;
	}

	// Update is called once per frame
	void Update()
	{
		//retreat Camera
		SpawnReference.DOLocalMove(DefaultTranslation + Vector3.back * TranslationByEntities * Entity.Count,0.3f);
		 
		//Combinaison keyBoard
		var index = 0;

		if (Input.GetKey(KeyCode.A))
			index += 32;
		if (Input.GetKey(KeyCode.Z))
			index += 16;
		if (Input.GetKey(KeyCode.E))
			index += 8;
		if (Input.GetKey(KeyCode.R))
			index += 4;
		if (Input.GetKey(KeyCode.T))
			index += 2;
		if (Input.GetKey(KeyCode.Y))
			index += 1;

		timeBeforeSpawn -= Time.deltaTime;
		if (timeBeforeSpawn <= 0)
		{
			if (index == 0)
				return;
			Debug.Log(index);
			timeBeforeSpawn = TimeBetweenSpawn;
			SpawnEntity(index);
		}
	}

	private void SpawnEntity(int index)
	{
		foreach (var c in PrefabEntities)
		{
			if (c.index == index)
			{
				SpawnEntity(c.EntityPrefab);
				return;
			}
		}
		Debug.LogWarning("Entities n°" + index + " not found.");
	}

	private void SpawnEntity(Entity EntityPrefab)
	{
		Entity e = Instantiate(EntityPrefab.gameObject, SpawnReference.position, Quaternion.identity)
			.GetComponent<Entity>();
		e.SetPlanet(Planet);
	}

/*
1 2 3 ou 4 pour une combinaison parmi 6
delay pendant laquelle il met en place sa combinaison, lecture des input à la fin du delay , spawn 
Pas de suivi en temps réel de la combinaison
4 type d'objets : normaux destructeur, terrain, etre vivant
camera qui dezoom
*/
}

[System.Serializable]
public struct Combinaison
{
	public int index;
	public Entity EntityPrefab;
}