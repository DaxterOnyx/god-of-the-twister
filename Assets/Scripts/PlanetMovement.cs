﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMovement : MonoBehaviour
{
public float RotationSpeed = 2f;
    public float Rs2 = .1f;
    public float Rs3 = .2f;

    // Update is called once per frame
    void Update()
    {
        //TODO Rotate camera and spawn reference for ignore force bug
        transform.Rotate(Rs2,RotationSpeed*Time.deltaTime,Rs3);
    }
}
