﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class LivingCreature : MonoBehaviour
{
    // à un intervalle de temps random (court), l'entité subit un mouvement de rotation dans une direction random
    // à un intervalle de temps random, l'entité s'élance dans la direction qu'elle regarde

    private Rigidbody _rigidbodyCommeMADitBrice;

    public float minIdleTimeMove = 1.0f;
    public float maxIdleTimeMove = 2.0f;
    private float timeCounterMove;
    private float idleTimeMove;
    public float minMoveForce = 200.0f;
    public float maxMoveForce = 600.0f;

    public float minIdleTimeJump = 60.0f;
    public float maxIdleTimeJump = 180.0f;
    private float timeCounterJump;
    private float idleTimeJump;
    public float minJumpForce = 15.0f;
    public float maxJumpForce = 30.0f;





    // Start is called before the first frame update
    void Start()
    {
        idleTimeMove = Random.Range(minIdleTimeMove, maxIdleTimeMove);
        idleTimeJump = Random.Range(minJumpForce, maxJumpForce);

        timeCounterMove = 0.0f;
        timeCounterJump = Random.Range(0.0f, minIdleTimeJump-10.0f); // grâce à cette ligne le jump peut être là relativement tôt dans la vie de l'objet
        
        Debug.Log(timeCounterJump);

        _rigidbodyCommeMADitBrice = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        timeCounterMove += Time.deltaTime;
        timeCounterJump += Time.deltaTime;

        if (timeCounterMove >= idleTimeMove)
        {
            Vector3 coucou = Random.onUnitSphere * Random.Range(minMoveForce, maxMoveForce);
            _rigidbodyCommeMADitBrice.AddForce(coucou, ForceMode.Force);

            timeCounterMove = 0.0f;
            idleTimeMove = Random.Range(minIdleTimeMove, maxIdleTimeMove);
        }

        if (timeCounterJump >= idleTimeJump)
        {
            Vector3 coucou = Random.onUnitSphere * Random.Range(minJumpForce, maxJumpForce);
            _rigidbodyCommeMADitBrice.AddForce(coucou, ForceMode.Impulse);

            timeCounterJump = 0.0f;
            idleTimeMove = Random.Range(minIdleTimeJump, maxIdleTimeJump);
        }
    }

}
