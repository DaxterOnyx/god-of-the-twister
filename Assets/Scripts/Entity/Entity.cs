﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Content;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody))]
public class Entity : MonoBehaviour
{
    public static int Count = 0;
    
    private PlanetMovement _planet;
    private Rigidbody _rigidbody;

    [SerializeField]
    private float gravityForce = 9.81f;

    // Start is called before the first frame update
    void Start()
    {
        Count++;
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        _rigidbody.AddForce((_planet.transform.position - transform.position).normalized * gravityForce,ForceMode.Force);
    }

    public void SetPlanet(PlanetMovement planet)
    {
        _planet = planet;
    }

    public void Destruct()
    {
        Count--;
        Destroy(gameObject);
    }
}
