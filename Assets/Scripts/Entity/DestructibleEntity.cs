﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleEntity : Entity
{
    public float AreaOfEffect = 0.5f;

    private void OnCollisionEnter(Collision other)
    {
       var a = Physics.OverlapSphere(transform.position, AreaOfEffect);
       foreach (var collider1 in a)
       {
	       var entity = collider1.GetComponent<Entity>();
	       
	       entity?.Destruct();
       }
       Destruct();
    }
}
